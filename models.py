from copy import deepcopy

import numpy as np


# TODO make it more abstract
class Variable:

    def __init__(self, value: int, constant: bool, scope: list, i: int, j: int):
        self.value = value
        self.constant = constant
        self.scope = scope
        self.i = i
        self.j = j
        self.blocked_values = []  # When we pop variable we don't want to have the same value

    def __str__(self):
        return str(self.value)

    def __repr__(self):
        return str(self.value)


class Sudoku:

    def __init__(self, sudoku_id, difficulty, puzzle):
        self.sudoku_id = sudoku_id
        self.difficulty = difficulty
        self.puzzle = self.init_puzzles(puzzle)
        self.variables = self.puzzle.reshape(81)

    def init_puzzles(self, puzzle):
        return np.array([[Variable(int(sign), True, [int(sign)], row, j) if sign.isdigit()
                          else Variable(0, False, list(range(1, 10)), row, j)
                          for j, sign in enumerate(puzzle[row + row * 8:row + row * 8 + 9])]
                         for row in range(9)])

    def is_allowed(self, variable: Variable, value: int, current_puzzle: list):
        current_puzzle = np.array([[current_puzzle[row * 9 + col].value if row * 9 + col < len(current_puzzle)
                                    else self.puzzle[row, col].value
                                    for col in range(9)]
                                   for row in range(9)])
        if not variable.constant:
            if value in current_puzzle[variable.i] or \
                    value in current_puzzle[:, variable.j] or \
                    value in self.find_square(variable.i, variable.j, current_puzzle).reshape(9):
                return False
            return True
        return True

    def find_square(self, i: int, j: int, current_puzzle):
        for row in range(0, 9, 3):
            for column in range(0, 9, 3):
                if row <= i < row + 3 and column <= j < column + 3:
                    return current_puzzle[row:row + 3, column:column + 3]

    def restrict_scopes(self, variable: Variable, value: int, list_of_variables):
        puzzle_with_modified_scopes = list_of_variables.reshape(9, 9)
        row = puzzle_with_modified_scopes[variable.i]
        column = puzzle_with_modified_scopes[:, variable.j]
        square_list = self.find_square(variable.i, variable.j, puzzle_with_modified_scopes).reshape(9)
        modified_scopes_rows = []
        for v in row:
            if v.j != variable.j:
                if v.constant and value == v.value:
                    return False
                reduced_scope = [s for s in v.scope if s != value]
                if len(reduced_scope) == 0:
                    return False
                modified_scopes_rows.append(reduced_scope)
        modified_scopes_cols = []
        for v in column:
            if v.i != variable.i:
                if v.constant and value == v.value:
                    return False
                reduced_scope = [s for s in v.scope if s != value]
                if len(reduced_scope) == 0:
                    return False
                modified_scopes_cols.append(reduced_scope)
        modified_scopes_square = []
        for v in square_list:
            if not (v.i == variable.i and v.j == variable.j):
                if v.constant and value == v.value:
                    return False
                reduced_scope = [s for s in v.scope if s != value]
                if len(reduced_scope) == 0:
                    return False
                modified_scopes_square.append(reduced_scope)
        index = 0
        for v in row:
            if v.j != variable.j:
                v.scope = modified_scopes_rows[index]
                index += 1
        index = 0
        for v in column:
            if v.i != variable.i:
                v.scope = modified_scopes_cols[index]
                index += 1
        index = 0
        for v in square_list:
            if not (v.i == variable.i and v.j == variable.j):
                v.scope = modified_scopes_square[index]
                index += 1
        return True

    def return_value(self, variable: Variable, value: int, list_of_variables):
        variable.value = 0
        puzzle_with_modified_scopes = list_of_variables.reshape(9, 9)
        row = puzzle_with_modified_scopes[variable.i]
        column = puzzle_with_modified_scopes[:, variable.j]
        square_list = self.find_square(variable.i, variable.j, puzzle_with_modified_scopes).reshape(9)
        for v in row:
            if not v.constant and value not in v.scope and self.is_allowed(v, value, list_of_variables):
                v.scope.append(value)
        for v in column:
            if not v.constant and value not in v.scope and self.is_allowed(v, value, list_of_variables):
                v.scope.append(value)
        for v in square_list:
            if not v.constant and value not in v.scope and self.is_allowed(v, value, list_of_variables):
                v.scope.append(value)


class Point:

    def __init__(self, row, col):
        self.row = row
        self.col = col


# TODO make it more abstract
class VariableJolka:

    def __init__(self, value, scope, is_row: bool, start_point: Point, end_point: Point):
        self.value = value
        self.scope = scope
        self.is_row = is_row
        self.blocked_values = []
        self.start_point = start_point
        self.end_point = end_point

    def __repr__(self):
        return str(self.value)

    def __str__(self):
        return str(self.value)


class Jolka:

    def __init__(self, puzzle, words):
        self.puzzle = self.init_puzzles(puzzle)
        self.variables = np.array(self.init_variables(self.puzzle, words) +
                                  self.init_variables(self.puzzle.T, words, transpose=True))
        self.words = words

    def init_variables(self, puzzle, words, transpose=False):
        vars_to_return = []
        counter_of_length = 0
        for i, row in enumerate(puzzle):
            for j, cell in enumerate(row):
                if cell == "_":
                    counter_of_length += 1
                if cell == "#" or j == len(row) - 1:
                    if counter_of_length != 0:
                        scope = []
                        for w in words:
                            if len(w) == counter_of_length:
                                scope.append(w)
                        if len(scope) != 0:
                            if not transpose:
                                vars_to_return.append(VariableJolka(counter_of_length * '_', scope, True,
                                                                    Point(i,
                                                                          j - counter_of_length if cell == "#" else j - counter_of_length + 1),
                                                                    Point(i, j - 1 if cell == "#" else j)))
                            else:
                                vars_to_return.append(VariableJolka(counter_of_length * '_', scope, False,
                                                                    Point(
                                                                        j - counter_of_length if cell == "#" else j - counter_of_length + 1,
                                                                        i),
                                                                    Point(j - 1 if cell == "#" else j, i)))
                    counter_of_length = 0
        return vars_to_return

    def is_allowed(self, variable: VariableJolka, value: str, current_puzzle: list):
        current_puzzle = self.build_puzzle_from_list_of_vars(current_puzzle)
        place_to_put = current_puzzle[variable.start_point.row: variable.end_point.row + 1,
                       variable.start_point.col: variable.end_point.col + 1]
        if variable.start_point.col - variable.end_point.col == 0:
            place_to_put = place_to_put.reshape(1, place_to_put.shape[0])
        for i in range(len(value)):
            if place_to_put[0][i] != '_' and place_to_put[0][i] != value[i]:
                return False
        return True

    def init_puzzles(self, puzzle):
        return np.array([[sign for sign in row] for row in puzzle])

    def build_puzzle_from_list_of_vars(self, list_of_vars):
        puzz = deepcopy(self.puzzle)
        for variable in list_of_vars:
            for i in range(variable.start_point.row, variable.end_point.row + 1):
                for j in range(variable.start_point.col, variable.end_point.col + 1):
                    if variable.start_point.row - variable.end_point.row == 0:
                        if puzz[i][j] == '_':  # TODO Added this check if still working, it makes it longer or not working on puzzle1
                            puzz[i][j] = variable.value[j - variable.start_point.col]
                    elif puzz[i][j] == '_':  # TODO Added this check if still working, it makes it longer or not working on puzzle1
                        puzz[i][j] = variable.value[i - variable.start_point.row]
        return puzz

    def restrict_scopes(self, variable: VariableJolka, value: str, list_of_variables):
        cols = [v for v in list_of_variables if not v.is_row]
        if variable.is_row:
            rows = [v for v in list_of_variables if v.is_row]
            modified_scopes_rows = []
            for v in rows:
                if len(v.value) == len(value) and (v.start_point.row != variable.start_point.row or
                                                   v.start_point.col != variable.start_point.col):
                    reduced_scope = [s for s in v.scope if s != value]
                    if len(reduced_scope) == 0:
                        return False
                    modified_scopes_rows.append(reduced_scope)

            modified_scopes_cols = []
            copy_to_check_is_allowed = deepcopy(list_of_variables)
            index_of_current_variable = -1
            for i, v in enumerate(list_of_variables):
                if v.start_point.row == variable.start_point.row and \
                        v.start_point.col == variable.start_point.col and \
                        v.end_point.row == variable.end_point.row and \
                        v.end_point.col == variable.end_point.col:
                    index_of_current_variable = i
                    break
            copy_to_check_is_allowed[index_of_current_variable].value = value
            for v in cols:
                reduced_scope = [s for s in v.scope if s != value and self.is_allowed(v, s, copy_to_check_is_allowed)]
                if len(reduced_scope) == 0:
                    return False
                modified_scopes_cols.append(reduced_scope)
            index = 0
            for v in rows:
                if len(v.value) == len(value) and (variable.start_point.row != v.start_point.row or
                                                   variable.start_point.col != v.start_point.col):
                    v.scope = modified_scopes_rows[index]
                    index += 1
            index = 0
            for v in cols:
                v.scope = modified_scopes_cols[index]
                index += 1
            return True
        else:
            modified_scopes_cols = []
            for v in cols:
                if len(v.value) == len(value) and (variable.start_point.row != v.start_point.row or
                                                   variable.start_point.col != v.start_point.col):
                    reduced_scope = [s for s in v.scope if s != value]
                    if len(reduced_scope) == 0:
                        return False
                    modified_scopes_cols.append(reduced_scope)
            index = 0
            for v in cols:
                if len(v.value) == len(value) and (variable.start_point.row != v.start_point.row or
                                                   variable.start_point.col != v.start_point.col):
                    v.scope = modified_scopes_cols[index]
                    index += 1
            return True

    def return_value(self, variable: VariableJolka, value: str, list_of_variables):
        variable.value = len(variable.value) * '_'
        for v in list_of_variables:
            for w in self.words:
                if len(w) == len(v.value) and w not in v.scope and self.is_allowed(v, w, list_of_variables):
                    v.scope.append(w)
