import numpy as np
import csv

from forward import solve_forward
from models import Sudoku, Jolka
from solver import solve


def read_data_sudoku():
    f = open('ai-lab2-2020-dane/Sudoku.csv')
    sudoku_list = []
    for line in f.readlines():
        if line[0].isdigit():
            sudoku_id, difficulty, puzzle, _ = line.split(sep=';')
            sudoku_list.append(Sudoku(sudoku_id, difficulty, puzzle))
    f.close()
    return sudoku_list


def read_data_jolka(number: int):
    f = open('ai-lab2-2020-dane/Jolka/puzzle' + str(number))
    puzzle = [line.rstrip() for line in f]
    f.close()
    f = open('ai-lab2-2020-dane/Jolka/words' + str(number))
    words = [word.rstrip() for word in f]
    f.close()
    return Jolka(np.array(puzzle), np.array(words))


if __name__ == '__main__':
    jolka = read_data_jolka(1)
    sol, d = solve_forward(jolka)
    print(sol)
    print(d)
    # sudokus = read_data_sudoku()
    # Sud = sudokus[40]
    # sol, _ = solve_forward(Sud)
    # print(len(sol))


    # TODO back searching
    # sudokus = read_data_sudoku()
    # Sud = sudokus[40]
    # solution, data_to_save = solve(Sud)
    # try:
    #     with open('data_sudoku_test.csv', 'w') as csvfile:
    #         writer = csv.DictWriter(csvfile, data_to_save.keys(), delimiter=';')
    #         writer.writeheader()
    #         writer.writerow(data_to_save)
    # except IOError:
    #     print("I/O error")

    # jolka = read_data_jolka(1)
    # solution_jolka, data_to_save_jolka = solve(jolka)
    # print(solution_jolka)
    # try:
    #     with open('data_jolka_test_0.csv', 'w') as csvfile:
    #         writer = csv.DictWriter(csvfile, data_to_save_jolka.keys(), delimiter=';')
    #         writer.writeheader()
    #         writer.writerow(data_to_save_jolka)
    # except IOError:
    #     print("I/O error")

