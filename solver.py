# Variables must have value and scope
# Where scope is list of allowed values
# value is current value of variable
from copy import deepcopy
import time


def solve(problem):
    start_time = time.time()
    data_to_save = {}  # Only for tracking information
    number_of_nodes_to_first = 0  # Only for tracking information
    number_of_all_nodes = 0  # Only for tracking information
    number_of_comebacks_to_first = 0  # Only for tracking information
    number_of_all_comebacks = 0  # Only for tracking information
    solutions = []
    variables = deepcopy(problem.variables)
    var_index = 0  # Only for selecting
    last_variable = []
    turn_back = False
    looking_for_variables = True
    while looking_for_variables:
        val_index = 0
        # Selecting variable
        variable = last_variable.pop() if turn_back else variables[var_index]
        if turn_back:  # Only for tracking information
            number_of_all_comebacks += 1  # Only for tracking information
            if len(solutions) == 0:  # Only for tracking information
                number_of_comebacks_to_first += 1  # Only for tracking information
        while turn_back and len(variable.scope) == 1:  # Scope with one value will cause inf loop
            if len(last_variable) == 0:
                break  # Stop algorithm, no solution
            variable = last_variable.pop()
            var_index -= 1  # Only for selecting
            number_of_all_comebacks += 1  # Only for tracking information
            if len(solutions) == 0:  # Only for tracking information
                number_of_comebacks_to_first += 1  # Only for tracking information
        if turn_back:
            variable.blocked_values.append(variable.value)
        scope = variable.scope
        still_unused_value = True
        while still_unused_value:
            if val_index == len(scope):  # Condition if all values were used
                still_unused_value = False
                var_index -= 1  # Only for selecting
                turn_back = True
                looking_for_variables = len(last_variable) != 0  # Stop algorithm, no solution
                variable.blocked_values = []  # Clear after checking all possibilities and go to node before
            else:
                # Selecting value
                if len(solutions) == 0:  # Only for tracking information
                    number_of_nodes_to_first += 1  # Only for tracking information
                number_of_all_nodes += 1  # Only for tracking information
                actual_value = scope[val_index]
                if problem.is_allowed(variable, actual_value, last_variable) and \
                        actual_value not in variable.blocked_values:
                    variable.value = actual_value
                    last_variable.append(variable)
                    still_unused_value = False
                    turn_back = False
                    var_index += 1  # Only for selecting
                    if len(solutions) == 1:  # Only for tracking information
                        data_to_save["time_to_find_first"] = time.time() - start_time  # Only for tracking information
                        data_to_save["number_of_nodes_to_first"] = number_of_nodes_to_first  # Only for tracking information
                        data_to_save["number_of_comebacks_to_first"] = number_of_comebacks_to_first  # Only for tracking information
                    if len(variables) == len(last_variable):  # Found solution
                        solutions.append(deepcopy(last_variable))
                        still_unused_value = True
            val_index += 1  # Used one more value
    data_to_save["number_of_all_comebacks"] = number_of_all_comebacks  # Only for tracking information
    data_to_save["number_of_all_nodes"] = number_of_all_nodes  # Only for tracking information
    data_to_save["time_of_all_solutions"] = time.time() - start_time  # Only for tracking information
    data_to_save["number_of_all_solutions"] = len(solutions)  # Only for tracking information
    return solutions, data_to_save
